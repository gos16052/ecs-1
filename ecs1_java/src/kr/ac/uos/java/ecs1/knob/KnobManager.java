package kr.ac.uos.java.ecs1.knob;

public class KnobManager 
{
	public static final int MIN_NUM_OF_KNOBS = 1;
	public static final int MAX_NUM_OF_KNOBS = 7;
	
	private Knob[] knobs = new Knob[7];
	private int numOfKnobs = 0;
	
	public KnobManager()
	{
		setNumOfKnobs(1);
	}
	
	public void setNumOfKnobs(int num)
	{
		if(KnobManager.MIN_NUM_OF_KNOBS <= num && num < KnobManager.MAX_NUM_OF_KNOBS)
		{
			if(numOfKnobs < num)
			{
				for(int i = numOfKnobs; i < num; i++)
				{
					knobs[i] = new Knob();
				}
			}
			numOfKnobs = num;
		}
	}
	
	public int getNumOfKnobs()
	{
		return this.numOfKnobs;
	}
	
	public Knob getKnobByIndex(int index)
	{
		if(KnobManager.MIN_NUM_OF_KNOBS <= index + 1 && index + 1 < KnobManager.MAX_NUM_OF_KNOBS)
		{
			return this.knobs[index];
		}
		return null;
	}
}
