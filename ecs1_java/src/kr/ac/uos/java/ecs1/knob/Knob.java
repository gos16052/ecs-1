package kr.ac.uos.java.ecs1.knob;

import java.io.Serializable;

public class Knob implements Serializable
{
	private static final long serialVersionUID = 1104819433272901220L;
	public static final int MAX_VALUE = 100;
	public static final int MIN_VALUE = 0;
	
	private int value = 0;
	private int interval = 0;
	private String name;
	
	public Knob()
	{
		this(0);
	}

	public Knob(int interval)
	{
		this.interval = interval;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setValue(int value)
	{		
		this.value = adjustValue(value);
	}
	
	public int getValue()
	{
		return this.value;
	}

	private int adjustValue(int value)
	{
		return (this.interval != 0) ? (value - value % this.interval) : (value);
	}
	
	public static boolean isBoundary(int v)
	{
		if(MIN_VALUE <= v && v <= MAX_VALUE)
		{
			return true;
		}
		return false;
	}
}
