package kr.ac.uos.java.ecs1.SerialConnection;

import java.io.IOException;
import java.io.OutputStream;

class SerialWriter implements Runnable
{
	OutputStream out;

	SerialWriter(OutputStream out)
	{
		this.out = out;
	}

	public void run()
	{
		System.out.println("SerialWriter ");
		try
		{
			int c = 0;
			while ((c = System.in.read()) > -1)
			{
				// System.out.println("WRITE:"+c);
				this.out.write(c);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}