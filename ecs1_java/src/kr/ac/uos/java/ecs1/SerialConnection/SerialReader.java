package kr.ac.uos.java.ecs1.SerialConnection;

import java.io.IOException;
import java.io.InputStream;

class SerialReader implements Runnable {
	private InputStream in;
	private CircularQueue cQueue;
	
	private byte temp;
	private byte isTemp;
	
	SerialReader(InputStream in)
	{
		cQueue = new CircularQueue(1024);
		this.in = in;
	}

	public void run()
	{
		System.out.println("SerialReader ");
		byte[] buffer = new byte[1024];
		
		int len = -1;
		try
		{
			System.out.println("before while");
			while ((len = this.in.read(buffer)) > -1)
			{
				if (len > 0)
				{
					for(int i=0; i<len; i++)
					{
						if((0<=buffer[i]) && (buffer[i]<0x10))  System.out.print("0");
						System.out.format("%x ", buffer[i]);
					}
//					System.out.println();
					convertData(len, buffer);
				}
			}
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	void convertData(int len, byte[] buffer)
	{
		if(((len+isTemp) % 2) == 1)
		{
			if(isTemp == 1)
			{
				if(isDataHigh(this.temp))
				{
					cQueue.enqueue(this.temp, buffer[0]);
					cQueue.enqueueAll(buffer, 1, len);
					isTemp = 0;
				}
				else
				{// temp is low
					//discard temp
					cQueue.enqueueAll(buffer, 0, len);
					isTemp = 0;
				}
			}
			else
			{ // isTemp == 0
				if(isDataHigh(buffer[0]))
				{
					cQueue.enqueueAll(buffer, 0, len-1);
					this.temp = buffer[len-1];
					isTemp = 1;
				}
				else
				{// buffer[0] is low 
					//discard buffer[0]
					cQueue.enqueueAll(buffer, 1, len);
					isTemp = 0;
				}
			}
		}
		else
		{//((len+isTemp) % 2) == 0
			if(isTemp == 1)
			{
				if(isDataHigh(this.temp))
				{
					cQueue.enqueue(this.temp, buffer[0]);
					cQueue.enqueueAll(buffer, 1, len-1);
					temp = buffer[len-1];
					isTemp = 1;
				}
				else
				{//temp is low
					//discard previous temp
					cQueue.enqueueAll(buffer, 0, len-1);
					temp = buffer[len-1];
					isTemp = 1;
				}
			}
			else
			{// isTemp == 0
				if(isDataHigh(buffer[0]))
				{
					cQueue.enqueueAll(buffer, 0, len);
					isTemp = 0;
				}
				else
				{// buffer[0] is low
					//discard buffer[0]
					cQueue.enqueueAll(buffer, 1, len-1);
					temp = buffer[len-1];
					isTemp = 1;
				}
			}
		}
	}
	boolean isDataHigh(byte data)
	{
		return (data & 0x80) == 0x80;
	}
	short readValue()
	{
		return this.cQueue.dequeue();
	}
}

class CircularQueue {
    private short[] arr;
    private int capacity;
    private int front;
    private int rear;
 
    CircularQueue(int capacity)
    {
        this.capacity = capacity;
        arr = new short[capacity + 1];
    }
 
    void enqueueAll(byte[] arr, int start, int end)
    {
    	if(start == end)
    	{
    		return;
    	}
    	
    	for(int i=start; i<end; i+=2)
    	{
    		this.enqueue(arr[i], arr[i+1]);
    	}
    }
    
    void enqueue(byte high, byte low)
    {
//    	System.out.format("phigh: 2x", high);
    	high = (byte) (high & (~0x80));
//    	System.out.format("high: 2x\t", high);
//    	System.out.format("low: 2x\n", low);
//    	System.out.format("res: 2x\n", (short)((short)(high<<4)|(short)(low&0x1f)));
    	this.enqueue((short)((short)(high<<4)|(short)(low&0x1f)));
    }
    
    void enqueue(short data)
    {
        int position = 0;
 
        
        if (rear == capacity + 1)
        {// 큐의 후방(rear)이 배열을 벗어났다면
            rear = 0;
            position = 0;
        }
        else
		{
        	position = rear++;
		}
 
        this.arr[position] = data;
    }
 
    short dequeue()
    {
        int position = front;
        if(isEmpty()) throw new IndexOutOfBoundsException();
        if (front == capacity)
        {// 큐의 전방(front)이 배열 끝에 위치해있으면
            front = 0;
        }
        else
        {
        	front++;
        }
//        System.out.print("sz[" + getSize()+"]");
        return arr[position];
    }
 
    int getSize() {
        if (front <= rear)
        {// 전방 index가 후방 index보다 앞에 위치해 있다면
	        return rear - front;
        }
        
        return (capacity + 1) - front + rear;
    }
 
    boolean isEmpty()
    {
        return front == rear;
    }
 
    boolean isFull()
    {
        if (front < rear)
        {// 전방 index가 후방 index보다 앞에 위치해 있다면
	        return (rear - front) == capacity;
        }
        return (rear + 1) == front;
    }
 
    int getCapacity()
    {
        return capacity;
    }
 
    int getFront()
    {
        return front;
    }
 
    int getRear()
    {
    	return rear;
    }
}
