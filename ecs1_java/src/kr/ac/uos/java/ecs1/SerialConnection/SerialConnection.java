package kr.ac.uos.java.ecs1.SerialConnection;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;

public class SerialConnection
{
	private final String PORT = "COM3";
	private SerialReader reader;
	
	public SerialConnection()
	{
		Iterator<CommPortIdentifier> h = getAvailableSerialPorts().iterator();
		CommPortIdentifier c;
		String portName = null;
		
		while(h.hasNext())
		{
			c = h.next();
			_println("{Name: " + c.getName() + ", PortType: " + c.getPortType() + "}");
			portName = c.getName();
			if(portName.equals(PORT)) break;
		}
		
		if(portName == null) return;
		
		try
		{
			connect(portName);	
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void _println(String s)
	{
		System.out.println(s);
	}
	
	void connect(String portName) throws Exception
	{
		CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
		
		if (portIdentifier.isCurrentlyOwned())
		{
			_println("Error: Port is currently isn use");
		}
		else
		{
			CommPort commPort = portIdentifier.open(this.getClass().getName(), 2000);
			//The 2000 value is the timeout value that is given to the system to release the port (2000 ms = 2 seconds). 
			if (commPort instanceof SerialPort)
			{
				_println("connect");
				SerialPort serialPort = (SerialPort) commPort;
				serialPort.setSerialPortParams(250000, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_ODD);
						//setSerialPortParams(Baudrate, DATABITS, STOPBITS, ISPARITY)
				InputStream in = serialPort.getInputStream();
				reader = new SerialReader(in);
				(new Thread(reader)).start();
				
//				OutputStream out = serialPort.getOutputStream();
//				(new Thread(new SerialWriter(out))).start();
			}
			else
			{
				_println("Error: only serial ports are handled by this example.");
			}
		}
	}

	public short readValue()
	{
		return reader.readValue();
	}

	public static void listPorts()
	{
		@SuppressWarnings("unchecked")
		Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();
		while(portEnum.hasMoreElements())
		{
			CommPortIdentifier portIdentifier = portEnum.nextElement();
            System.out.println(portIdentifier.getName()  +  " - " +  getPortTypeName(portIdentifier.getPortType()) );
		}
	}
	
	public static String getPortTypeName(int portType)
	{
		switch(portType)
		{
		case CommPortIdentifier.PORT_I2C:
			return "I2C";
		case CommPortIdentifier.PORT_PARALLEL:
			return "Parallel";
		case CommPortIdentifier.PORT_RAW:
			return "Raw";
		case CommPortIdentifier.PORT_RS485:
			return "RS485";
		case CommPortIdentifier.PORT_SERIAL:
			return "Serial";
		default:
			return "unknown type";
		}
	}
	
	public static HashSet<CommPortIdentifier> getAvailableSerialPorts()
	{
		HashSet<CommPortIdentifier> h = new HashSet<CommPortIdentifier>();
		@SuppressWarnings("unchecked")
		Enumeration<CommPortIdentifier> ports = CommPortIdentifier.getPortIdentifiers();
		while(ports.hasMoreElements())
		{
			CommPortIdentifier com = (CommPortIdentifier) ports.nextElement();
			if(com.getPortType() == CommPortIdentifier.PORT_SERIAL)
			{
				try
				{
					CommPort port = com.open("CommUtil", 50);
					port.close();
					h.add(com);
				}
				catch(PortInUseException e)
				{
                    System.out.println("Port, "  + com.getName() + ", is in use.");
				}
				catch(Exception e)
				{
                    System.err.println("Failed to open port " +  com.getName());
                    e.printStackTrace();
				}
			}
		}
		return h;
	}	
}
