package kr.ac.uos.java.ecs1.boundaries.mainform;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.plaf.basic.BasicScrollBarUI;

import kr.ac.uos.java.ecs1.knob.Knob;

public class KnobScrBar extends JScrollBar
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9098014864153038147L;
	private static Dimension dim = new Dimension(20,100);
	private JLabel knobValueLabel;
	
	public KnobScrBar(JLabel knobValueLabel)
	{
		super(JScrollBar.VERTICAL, 0, 10, Knob.MIN_VALUE, Knob.MAX_VALUE+20);
		this.knobValueLabel = knobValueLabel;
		this.setSize(dim);
		this.setPreferredSize(dim);

		this.addAdjustmentListener(
			new AdjustmentListener() 
			{
				@Override
				public void adjustmentValueChanged(AdjustmentEvent arg) 
				{
					knobValueLabel.setText(String.valueOf(arg.getValue()));
				}
			}
		); // scroll bar value changed listener

		this.setBorder(BorderFactory.createLineBorder(Color.black));
		this.setUI(
			new BasicScrollBarUI()
			{
				Dimension dim = new Dimension(0,0);
				@Override
				protected JButton createDecreaseButton(int orientation)
				{
					return createZeroButton();
				}
				@Override
				protected JButton createIncreaseButton(int orientation)
				{
					return createZeroButton();
				}
				
				private JButton createZeroButton()
				{
					JButton jbtn = new JButton();
					
					jbtn.setPreferredSize(dim);
					jbtn.setMinimumSize(dim);
					jbtn.setMaximumSize(dim);
					return jbtn;
				}
			}
		); // remove arrow button
	}
	
	private int adjustValue(int value)
	{
		return Knob.MAX_VALUE - value;
	}
	
	@Override
	public void setValue(int value)
	{
		super.setValue(adjustValue(value));
	}
	
	public void readFromKnob(Knob knob)
	{
		this.setValue(knob.getValue());
		this.setName(knobValueLabel.getText());
	}
}
