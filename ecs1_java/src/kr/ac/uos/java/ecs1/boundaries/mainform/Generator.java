package kr.ac.uos.java.ecs1.boundaries.mainform;

import java.awt.Dimension;

import javax.swing.JLabel;

public class Generator {
	public static JLabel generateLabel(String name, int width, int height)
	{
		Dimension dim = new Dimension(width, height);
		JLabel label = new JLabel(name);
		label.setPreferredSize(dim);
		label.setSize(dim);
		return label;
	}
}
