package kr.ac.uos.java.ecs1.boundaries.mainform;

import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class LowerPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3234563480584435098L;
	private GridPanel gridPanel;
	private SettingPanel settingPanel;
	
	
	public LowerPanel()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		this.gridPanel = new GridPanel();
		this.settingPanel = new SettingPanel(this.gridPanel);

		this.add(this.gridPanel);
		this.add(this.settingPanel);
		setEnabled(false);
	}
	
	public void setLabel(String text)
	{
		this.settingPanel.setLabel(text);
	}
	
	public void setEnabled(boolean enabled, boolean isEffector)
	{
		if(isEffector)
		{
			super.setEnabled(enabled);
			settingPanel.setEnabled(true, true);
			return;
		}
		setEnabled(enabled);
	}
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for(Component c : getComponents())
		{
			c.setEnabled(enabled);
		}
	}
}