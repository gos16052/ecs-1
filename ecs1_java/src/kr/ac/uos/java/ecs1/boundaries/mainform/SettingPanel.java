package kr.ac.uos.java.ecs1.boundaries.mainform;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SettingPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 647983123657618365L;
//	@SuppressWarnings("rawtypes")
	private JComboBox<Integer> numOfKnobsCBox;
	private JLabel stateLabel = new JLabel("__________");
	private JButton buttonStart;
	private JButton buttonSave;
	
	public SettingPanel(GridPanel gridPanel)
	{
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		this.stateLabel.setPreferredSize(new Dimension(75,16));
		this.add(this.stateLabel);
		
		this.add(Generator.generateLabel(null, 0, 150));
		
		Integer[] list = { 1, 2, 3, 4, 5, 6, 7 };
		numOfKnobsCBox = new JComboBox<Integer>(list);
		numOfKnobsCBox.addItemListener(
			new ItemListener()
			{
				@Override
				public void itemStateChanged(ItemEvent event) {
					if(event.getStateChange() == ItemEvent.SELECTED)
					{
						gridPanel.setNumOfPanel(numOfKnobsCBox.getSelectedIndex() + 1);
					}
				}
			}
		);
		
		this.add(numOfKnobsCBox);
		this.add(Generator.generateLabel(null, 0, 100));
		
		this.buttonStart = new JButton("Start");
		this.buttonStart.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e) 
					{
						gridPanel.setToKnob();
					}
				}
			);
		this.add(this.buttonStart);
		this.add(Generator.generateLabel(null, 0, 40));
		
		this.buttonSave = new JButton("Save");
		this.buttonSave.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e) 
					{
						//save
					}
				}
		);
		
		this.add(this.buttonSave);
		this.add(Generator.generateLabel(null, 0, 40));
	}
	
	public void setLabel(String text)
	{
		this.stateLabel.setText(text);
	}

	public void setEnabled(boolean enabled, boolean isEffector)
	{
		if(isEffector)
		{
			stateLabel.setEnabled(true);
			buttonStart.setEnabled(enabled);
			return;
		}
		this.setEnabled(enabled);
	}
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for(Component c : getComponents())
		{
			c.setEnabled(enabled);
		}
	}
}
