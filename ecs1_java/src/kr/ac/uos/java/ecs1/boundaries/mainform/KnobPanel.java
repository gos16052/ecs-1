package kr.ac.uos.java.ecs1.boundaries.mainform;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import kr.ac.uos.java.ecs1.knob.Knob;

public class KnobPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7830579060058524329L;
	private KnobPanel thiz;
	private KnobScrBar scrBar;
	private Knob knob;
	private JLabel knobNameLabel;
	private JLabel knobValueLabel;
	private boolean isCreated;
	private JTextField setNameTextField;
	private JTextField setValueTextField;
	
	public KnobPanel()
	{
		this.isCreated = false;
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
	}
	
	public void createKnob()
	{
		this.thiz = this;
		this.isCreated = true;
		this.knobValueLabel = new JLabel(Integer.toString(100));
		this.knob = new Knob();
		this.scrBar = new KnobScrBar(this.knobValueLabel);
		this.add(this.scrBar);
		this.scrBar.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(Generator.generateLabel(null, 0, 10));
		
		this.add(this.knobValueLabel);
		this.add(Generator.generateLabel(null, 0, 10));
		this.knobValueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.knobValueLabel.addMouseListener(
			new MouseListener()
			{
				@Override
				public void mouseClicked(MouseEvent e)
				{
					if(e.getClickCount()==2)
					{
						if(setValueTextField == null)
						{
							setValueTextField = new JTextField();
							Dimension dim = new Dimension(100,25);
							setValueTextField.setPreferredSize(dim);
							setValueTextField.setSize(dim);
							setValueTextField.setMinimumSize(dim);
							setValueTextField.setMaximumSize(dim);
							setValueTextField.addKeyListener(
								new KeyListener()
								{
									@Override
									public void keyPressed(KeyEvent arg)
									{
										if(arg.getKeyCode() == KeyEvent.VK_ENTER)
										{
											try
											{ 
												String s = setValueTextField.getText();
												Integer t = Integer.parseInt(s); 
												if(Knob.isBoundary(t))
												{
													scrBar.setValue(t);
													knobValueLabel.setText(s);
												}
											}
											catch(NumberFormatException e)
											{ 
												return; 
											}
											
											thiz.setVisible(false);
											thiz.remove(setValueTextField);
											try{ thiz.remove(knobNameLabel); }
											catch(NullPointerException ex){}
											try{ thiz.remove(setNameTextField); }
											catch(NullPointerException ex){}
											thiz.add(knobValueLabel);
											thiz.add(knobNameLabel);
											thiz.setVisible(true);
											}
									}
									@Override
									public void keyTyped(KeyEvent arg){}
									@Override
									public void keyReleased(KeyEvent arg){}
								}
							);
						}
						thiz.setVisible(false);
						thiz.remove(knobValueLabel);
						try{ thiz.remove(knobNameLabel); }
						catch(NullPointerException ex){}
						try{ thiz.remove(setNameTextField); }
						catch(NullPointerException ex){}
						thiz.add(setValueTextField);
						thiz.add(knobNameLabel);
						thiz.setVisible(true);
					}
				}

				@Override
				public void mouseEntered(MouseEvent e) {}
				@Override
				public void mouseExited(MouseEvent e) {}
				@Override
				public void mousePressed(MouseEvent e) {}
				@Override
				public void mouseReleased(MouseEvent e) {}
			}
		);

		this.knobNameLabel = new JLabel("Click Here");
		this.add(this.knobNameLabel);
		this.add(Generator.generateLabel(null, 0, 10));
		this.knobNameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.knobNameLabel.setPreferredSize(new Dimension(0,20));
		this.knobNameLabel.addMouseListener(
			new MouseListener()
			{
				@Override
				public void mouseClicked(MouseEvent e)
				{
					if(e.getClickCount()==2)
					{ // double click
						if(setNameTextField == null)
						{
							setNameTextField = new JTextField();
							Dimension dim = new Dimension(100,25);
							setNameTextField.setPreferredSize(dim);
							setNameTextField.setSize(50,15);
							setNameTextField.setMinimumSize(dim);
							setNameTextField.setMaximumSize(dim);
							setNameTextField.addKeyListener(
								new KeyListener()
								{
									@Override
									public void keyPressed(KeyEvent arg) {
										if(arg.getKeyCode() == KeyEvent.VK_ENTER)
										{
											knobNameLabel.setText(setNameTextField.getText());
											thiz.setVisible(false);
											thiz.remove(setNameTextField);
											try{ thiz.remove(knobValueLabel); }
											catch(NullPointerException ex){}
											try{ thiz.remove(setValueTextField); }
											catch(NullPointerException ex){}
											thiz.add(knobValueLabel);
											thiz.add(knobNameLabel);
											thiz.setVisible(true);
										}
									}
									@Override
									public void keyTyped(KeyEvent arg) {}
									@Override
									public void keyReleased(KeyEvent arg0) { }
																	}
							);
						}
						thiz.setVisible(false);
						try{ thiz.remove(knobValueLabel); }
						catch(NullPointerException ex){}
						try{ thiz.remove(setValueTextField); }
						catch(NullPointerException ex){}
						thiz.remove(knobNameLabel);
						thiz.add(knobValueLabel);
						thiz.add(setNameTextField);
						thiz.setVisible(true);
					}
				}
				@Override
				public void mouseEntered(MouseEvent e) {}
				@Override
				public void mouseExited(MouseEvent e) {}
				@Override
				public void mousePressed(MouseEvent e) {}
				@Override
				public void mouseReleased(MouseEvent e) {}
			}
		);
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for(Component c : getComponents())
		{
			c.setEnabled(enabled);
		}
	}
	
	public boolean isCreated()
	{
		return this.isCreated;
	}
	
	public void readFromKnob()
	{ // read knob name from knob
		this.knobNameLabel.setText(knob.getName());
		this.scrBar.readFromKnob(this.knob);
	}
	
	public void setToKnob()
	{
		knob.setValue(scrBar.getValue());
		knob.setName(this.knobNameLabel.getText());
	}
}
