package kr.ac.uos.java.ecs1.boundaries.mainform;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;

import kr.ac.uos.java.ecs1.knob.KnobManager;

public class GridPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2114071816217302380L;
	public static final int KNOB_ROW = 2;
	public static final int KNOB_COL = 4;
	
	private GridLayout gridMap;
	private KnobPanel[] knobPanels;
	
	private KnobManager knobCtlr;
	private int numOfKnobs;
	
	public GridPanel()
	{
		this.knobCtlr = new KnobManager();
		
		int k = 0;
		knobPanels = new KnobPanel[KNOB_ROW * KNOB_COL];
		for(int i = 0; i < KNOB_ROW; i++)
		{
			for(int j = 0; j < KNOB_COL; j++)
			{
				knobPanels[k] = new KnobPanel();
				this.add(knobPanels[k]);
				k++;
			}
		}
		UpdateGridPanel();
		
		this.gridMap = new GridLayout(KNOB_ROW,KNOB_COL);
		this.setPreferredSize(new Dimension(520,260));
		this.setLayout(this.gridMap);
	}
	
	private void UpdateGridPanel()
	{
		setVisible(false);
		int numOfKnobs = this.knobCtlr.getNumOfKnobs();
		if(this.numOfKnobs > numOfKnobs)
		{
			for(int i = this.numOfKnobs - 1; i >= numOfKnobs; i--)
			{
				this.knobPanels[i].setVisible(false);
			}
		}
		else
		{
			for(int i = this.numOfKnobs; i < numOfKnobs; i++)
			{
				if(knobPanels[i].isCreated() == false)
				{
					this.knobPanels[i].createKnob();
				}
				else
				{
					this.knobPanels[i].setVisible(true);
				}
			}
		}
		this.numOfKnobs = numOfKnobs;
		setVisible(true);
	}
	
	public void setNumOfPanel(int num)
	{
		knobCtlr.setNumOfKnobs(num);
		UpdateGridPanel();
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for(Component c : getComponents())
		{
			c.setEnabled(enabled);
		}
	}
	
	public void setToKnob()
	{
		for(KnobPanel p : this.knobPanels)
		{
			p.setToKnob();
		}
	}
}
