package kr.ac.uos.java.ecs1.boundaries.mainform;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.jfree.ui.RefineryUtilities;

import kr.ac.uos.java.ecs1.boundaries.plottingform.DynamicLineAndTimeSeriesChart;

public class UpperPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8438937833748881181L;
	private JPanel UpperPanel = this;
	private JButton buttonCopy = new JButton("Copy");
	private JButton buttonEffector = new JButton("Effector");
	private JButton buttonOsc = new JButton("Oscilloscope");
	private JFileChooser jfc = new JFileChooser();
	private LowerPanel lowerPanel;
	
	
	public UpperPanel(LowerPanel lPanel)
	{
		this.lowerPanel = lPanel;
		this.setLayout(new FlowLayout());
		this.setMaximumSize(new Dimension(300, 100));
		this.setMinimumSize(new Dimension(300, 100));
		this.setSize(300,100);
		this.add(this.buttonCopy);
		this.buttonCopy.addActionListener(
			new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e) 
				{
					lowerPanel.setEnabled(true);
					lowerPanel.setLabel("___COPY___");
				}
			}
		);
		
		this.add(this.buttonEffector);
		this.buttonEffector.addActionListener(
			new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(jfc.showOpenDialog(UpperPanel) == JFileChooser.APPROVE_OPTION)
					{
						String path = jfc.getSelectedFile().toString();
						System.out.println(path);
					}
					
					lowerPanel.setLabel("_EFFECTOR_");
					lowerPanel.setEnabled(false);
					lowerPanel.setEnabled(true,true);
				}
			}
		);
		
		this.add(this.buttonOsc);
		this.buttonOsc.addActionListener(
			new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					// I Found the Site-->
					// http://stackoverflow.com/questions/11469865/making-dynamic-line-chart-using-jfree-chart-in-java
			        final DynamicLineAndTimeSeriesChart demo = new DynamicLineAndTimeSeriesChart("Dynamic Line And TimeSeries Chart");
			        demo.pack();
			        RefineryUtilities.centerFrameOnScreen(demo);
			        demo.setVisible(true);
				}
			}
		);
		
		this.jfc.setMultiSelectionEnabled(false);
	}
}