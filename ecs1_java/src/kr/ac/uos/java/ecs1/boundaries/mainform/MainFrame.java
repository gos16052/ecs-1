package kr.ac.uos.java.ecs1.boundaries.mainform;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class MainFrame extends JFrame{
	private static final long serialVersionUID = -7023079440201164065L;
	private JPanel mainPanel = new JPanel();
	private JPanel upperPanel;
	private JPanel lowerPanel;

	public MainFrame()
	{
		setResizable(false);
		setSize(640,480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("ECS-1");
		initMainPanel();
		setVisible(true);
	}
	
	private void initMainPanel()
	{
		this.mainPanel.setLayout(new BoxLayout(this.mainPanel, BoxLayout.PAGE_AXIS));
		this.add(this.mainPanel);
		this.mainPanel.setSize(this.getSize());
		this.lowerPanel = new LowerPanel();
		this.upperPanel = new UpperPanel((LowerPanel)this.lowerPanel);
		this.mainPanel.add(this.upperPanel);
		this.mainPanel.add(this.lowerPanel);
	}
}
