#define SEGMENT_HIPEN 16
#define SEGMENT_POINT 17

//		digit values 			 	  0						  4						  8			  A	    B     C     D     E     F    "-"    "."   " "
unsigned const int seg_digit[19] = { 0x3f, 0x06, 0x5B, 0x4f, 0x66, 0x6D, 0x7D, 0x27, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71, 0x40, 0x80, 0x00};
unsigned const int seg_enable[4] = { 0x01, 0x02, 0x04, 0x08 };

// Set C port and G port 'Output state'
void segment_init()
{
	DDRC = 0xff; 
	DDRG = 0x0f;
}

void segment_display(short value)
{	
	volatile char i;
	char divide = 100;
	
	if(value < -999 || 999 < value) return;

	if(value < 0)
	{
		PORTG = seg_enable[3];
		PORTC = seg_digit[SEGMENT_HIPEN];
		value = - value;
		_delay_ms(1);
	}

	for(i=0; i<3; i++)
	{
		PORTC = seg_digit[value/divide];
		value = value % divide;
		divide = divide / 10;
		PORTG = seg_enable[2-i];
		_delay_ms(1);
	}
}

void segment_display_hex(short value)
{
	PORTG = seg_enable[3];
	PORTC = seg_digit[(value & 0xf000)>>12];
	_delay_ms(1);
	PORTG = seg_enable[2];
	PORTC = seg_digit[(value & 0x0f00)>>8];
	_delay_ms(1);
	PORTG = seg_enable[1];
	PORTC = seg_digit[(value & 0x00f0)>>4];
	_delay_ms(1);
	PORTG = seg_enable[0];
	PORTC = seg_digit[(value & 0x000f)];
	_delay_ms(1);
}
void segment_display_iter(short value, int n)
{
	volatile int i;
	for(i=0; i<n; i++)
	{
		segment_display((value > 511)? -(1023-value) : value);
	}
}

