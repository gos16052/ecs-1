void adc_init()
{
	ADMUX = 0x13; // AREF(+5V), right alignment, POSITIVE: ADC3, NEGATIVE: ADC1, GAIN: 1x

	ADCSRA = 0x87;
	// ADEN = 1, ADC Enable
	// ADFR = 1, single conversion mode
	// ADPS(2:0) = 0x111, prescaler 128
}

short adc_read()
{
	unsigned char adc_low, adc_high;
	unsigned short value;

	ADCSRA |= 0x40; //ADC start conversion, ADSC = '1'
	while((ADCSRA & 0x10) != 0x10); // check adc has been convert

	adc_low = ADCL;
	adc_high = ADCH;
	value = (adc_high << 8) | adc_low;

	return value;
}
