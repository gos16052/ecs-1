#include <avr/io.h>
#define F_CPU 16000000L
#define __DELAY_BACKWARD_COMPATIBLE__ 
#include <util/delay.h>
#define NULL 0

#include "segment.h"
#include "adc.h"
#include "SerialComm.h"

void serial_send_sadc(short adc);

int main()
{
	volatile short i;
	unsigned short adc_value;
	adc_init();
	serial_init();
	segment_init();

	while(1)
	{
//		adc_value = adc_read();
//		segment_display_iter(adc_value, 100);
//		for(i=0;i<100;i++) segment_display_hex(adc_value);
//		serial_send_sadc(adc_value);
		serial_send_sadc(i++);
		if(i==511) i=0;
	}

	return 0;
}


void serial_send_sadc(short adc)
{
//	volatile int i;
	byte adc_low = adc & 0x1f;
	byte adc_high = (adc & ~(0x1f))>>5;
	adc_high = adc_high | 0x80;
//	for(i=0;i<100;i++) segment_display_hex(adc_low | (adc_high<<8));

	serial_send_byte(adc_high);
	serial_send_byte(adc_low);
}
