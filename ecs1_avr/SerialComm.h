typedef char byte;

void serial_init()
{
	UBRR0H = 0; // 12bits meaning 
	UBRR0L = 7;
//	UBRR0L = 0x00; // 16Mhz, 2Mbps baudrate, ref. ATmega128 datasheet 
	UCSR0A = UCSR0A | 0x02; // 0000 0010, U2X on

	UCSR0B = 0x08; // Receive(RX) and Transmit(TX) Enable 
	UCSR0C = 0x36; // UART Mode, 8 Bit Data, No Parity, 1 Stop Bit, odd parity

}

// 1 byte send function
void serial_send_byte(byte c)
{
	while(!(UCSR0A & 0x20)); // UCSR0A 5th bit: UDRE
	UDR0 = c; // 1 byte send
	_delay_ms(1);
}

// 1byte receive function
byte serial_receive_byte()
{
	while (!(UCSR0A & 0x80)); // UCSR0A 7th bit: RXC(Receiver Complete)
	return(UDR0); // 1 byte receive
	_delay_ms(1);
}

#if 0
// string send function
void serial_send_string(char *ptr)
{
	while(1)
	{
		if (*ptr != NULL) // 1 byte transmit
			serial_send_byte(*ptr++);
		else
			return; // if eos, then return
	}
}
#endif
